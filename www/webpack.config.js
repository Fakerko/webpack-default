const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");


// load optional local config
const webpackConfLocalFile = 'webpack.config.local.js';
try {
	var webpackConfigLocal = require('./' + webpackConfLocalFile);
} catch (e) {
	if (e instanceof Error && e.code === "MODULE_NOT_FOUND") {
		console.log('No '+webpackConfLocalFile+' present, using default config.');
	} else {
		throw e;
	}
}


// path setup
const srcPath = './src/';
const distPath = './www/assets';

const PATHS = {
	src: path.join(__dirname, 'src')
  }


// BrowserSync options
var browserSyncOptions = {
	host: 'localhost',
	port: 3000,
	server: { baseDir: ['www'] }
};
if(webpackConfigLocal && webpackConfigLocal.browserSyncOptions){
	browserSyncOptions = webpackConfigLocal.browserSyncOptions;
	console.log('Loaded '+webpackConfLocalFile);
}


module.exports = (env, argv) => ({
	mode: process.env.NODE_ENV || 'development',

	entry: {
		main: [
			srcPath + 'js/main.js',
			srcPath + 'scss/main.scss'
		]
	},

	output: {
		path: path.resolve(__dirname, distPath),
		filename: 'js/[name].js'
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			}, 
			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'file-loader',
						options: {
							name: 'css/[name].css'
						}
					},
					{
						loader: 'extract-loader'
					},
					{
						loader: "css-loader"
					},
					{
						loader: "postcss-loader"
					},
					{
						loader: "sass-loader"
					}
				]
			}, 
			{
			test: /\.(png|jpg|jpeg|gif|svg)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[path][name].[ext]',
							context: path.resolve(__dirname, './src'),
							publicPath: '../'
						}
					}
				]
			},
			{
			test: /\.(woff|woff2|eot|ttf|otf)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[path][name].[ext]',
							context: path.resolve(__dirname, './src'),
							publicPath: '../'
						}
					}
				]
			}
		]
	},

	optimization: {
		minimize: true,
		minimizer: [
			(compiler) => {
				if(argv.mode === "production") {
					new TerserPlugin({
						terserOptions: {
							format: {
								comments: false,
							},
						},
						extractComments: (astNode, comment) => false,
					}).apply(compiler);
					new CssMinimizerPlugin().apply(compiler);
				}
			}
			
		]
	},

	plugins: [
		new BrowserSyncPlugin(browserSyncOptions),
		new MiniCssExtractPlugin({
			filename: "./css/main.css",
			chunkFilename: "main.css"
		}),
	]
	
});
