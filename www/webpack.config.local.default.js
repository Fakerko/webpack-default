var webpackConfigLocal = {

	browserSyncOptions: {
		host: 'localhost',
		port: 3000,
		reloadDelay: 100,
		//proxy: 'dev.example.com/directory/', // use hostname, optional subdir
		server: { baseDir: ['www'] }, // use local server
	}
	
};

module.exports = webpackConfigLocal;
