/* CONSTANTS */
export const TEMPLATE_HOME = "TEMPLATE_HOME";


/* COMPONENTS */
// import 'bootstrap/js/dist/alert';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/carousel';
// import 'bootstrap/js/dist/collapse';
// import 'bootstrap/js/dist/dropdown';
// import Modal from 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/popover';
// import 'bootstrap/js/dist/scrollspy';
// import 'bootstrap/js/dist/tab';
// import 'bootstrap/js/dist/toast';
// import 'bootstrap/js/dist/tooltip';


onDocumentReady(function() {
	let html = document.querySelector('html');
	let template = html.dataset.template;

	switch (template) {
		case TEMPLATE_HOME:
			/* JS for HOME */
			break;
	}
});

function onDocumentReady(fn) {
	if (document.readyState === "complete" || document.readyState === "interactive") {
		setTimeout(fn, 1);
	} else {
		document.addEventListener("DOMContentLoaded", fn);
	}
}    
